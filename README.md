 [![pipeline status](https://gitlab.com/ltacchini/pp2-2022-s2-gitlabci/badges/main/pipeline.svg)](https://gitlab.com/ltacchini/pp2-2022-s2-gitlabci/-/commits/main) 

# Integración Continua

Esta guía tiene como objetivo explicar los pasos a seguir para configurar la herramienta de integración continua “Gitlab CI/CD” integrada en la propia interfaz gráfica del repositorio.

Documentación de Gitlab: ****[https://docs.gitlab.com/ee/ci/quick_start/](https://docs.gitlab.com/ee/ci/quick_start/)

# Paso a paso con Java y Gradle

Para la realización de la presente guía se utilizará como ejemplo un proyecto Java que utiliza el gestor de dependencias a Gradle.

Gitlab-ci cuenta con entornos virtuales llamados *runners* que se encargan de preparar el ambiente de ejecución para poder correr el *build* y los casos de *test* del proyecto.

### Setup

1. Crear un proyecto Gradle en *Eclipse for Java Developers* (Version: 2018-12 (4.10.0) o superior).
2. Crear un repositorio en Gitlab y ejecutar git push del proyecto Gradle al repositorio.

### Activar Gitlab-CI

En la página del proyecto: 

- *Set up CI/CD*
- *Apply a Gitlab CI Yaml Template*
- *Gradle*
- *Commit changes*

## Configuración Multiproyecto

1. Entrar en [https://jitpack.io/](https://jitpack.io/)
2. Copiar la URL del proyecto que se desea agregar como dependencia (en este caso el proyecto A) -> Lookup -> Snapshot -> Get it -> Copiar y pegar el código generado para gradle.
3. En el build.gradle del proyecto B agregar la URL de jitpack.

```yaml
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

1. En el archivo build.gradle del proyecto B agregar la dependencia hacia el proyecto A y realizar commit and push del build.gradle.

```
dependencies {
      implementation 'com.gitlab.autor:gitlab-ci-test:-SNAPSHOT' 
}
```

## Testing

Para la presente guía utilizaremos el plugin “Junit” que se encargará de ejecutar los tests unitarios sobre nuestro código. Luego, estos reportes serán publicados por gitlab-ci.

Los distintos niveles de ejecución de gitlab-ci se representan con un *pipeline.* Cada fase de ejecución dentro del *pipeline* es llamado *stage* en el .yml y *job* en la interfaz gráfica de gitlab.

1. Ir a la URL del proyecto en Gitlab y selecciones “Set up CI/CD”:

```
# This file is a template, and might need editing before it works on your project.
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Gradle.gitlab-ci.yml

# This is the Gradle build system for JVM applications
# https://gradle.org/
# https://github.com/gradle/gradle

image: gradle:alpine

# Disable the Gradle daemon for Continuous Integration servers as correctness
# is usually a priority over speed in CI environments. Using a fresh
# runtime for each build is more reliable since the runtime is completely
# isolated from any previous builds.
variables:
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"

before_script:
  - GRADLE_USER_HOME="$(pwd)/.gradle"
  - export GRADLE_USER_HOME

build:
  stage: build
  script: gradle --build-cache assemble
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: push
    paths:
      - build
      - .gradle

test:
  stage: test
  script: gradle check
  cache:
    key: "$CI_COMMIT_REF_NAME"
    policy: pull
    paths:
      - build
      - .gradle
```
